package UF1;

import java.time.LocalDateTime;

public abstract class Tripulant {
    protected static final String bandol = "Imperi Klingon";
    protected String id;
    protected String nom;
    protected boolean actiu;
    protected int departament;
    protected int llocServei;
    protected LocalDateTime dataAlta;

    protected Tripulant(String id, String nom, boolean actiu, int departament, int llocServei, LocalDateTime dataAlta) {
        this.id = id;
        this.nom = nom;
        this.actiu = actiu;
        this.departament = departament;
        this.llocServei = llocServei;
        this.dataAlta = dataAlta;
    }



    abstract protected void imprimirDadesTripulant();


    protected int getLlocServei() {
        return llocServei;
    }

    protected void setLlocServei(int llocServei) {
        this.llocServei = llocServei;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Tripulant other = (Tripulant) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    protected void saludar() {
        System.out.println("Hola sub");
    }
}
