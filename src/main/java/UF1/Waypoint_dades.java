package UF1;

import java.time.LocalDateTime;
import java.util.Arrays;

public class Waypoint_dades {
    private int id;
    private String nom;
    private int[] cordenades;
    private boolean actiu;

    private LocalDateTime dataCreacio;
    private LocalDateTime dataAnulacio;
    private LocalDateTime dataModificacio;

    public Waypoint_dades(int id, String nom, int[] cordenades, boolean actiu, LocalDateTime dataCreacio, LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
        this.id = id;
        this.nom = nom;
        this.cordenades = cordenades;
        this.actiu = actiu;
        this.dataCreacio = dataCreacio;
        this.dataAnulacio = dataAnulacio;
        this.dataModificacio = dataModificacio;
    }

    @Override
    public String toString() {
        String txt = "";

        txt = "WAYPOINT " + id + ":" +
                "\n nom= " + nom +
                "\n cordenades(x,y,z) = (" + cordenades[0] +","+ cordenades[1]+","+ cordenades[2]+")"+
                ", actiu=" + actiu;

        if(dataCreacio == null) txt = txt + "\n dataCreacio = NULL" + dataCreacio;
        if(dataAnulacio == null) txt = txt + "\n dataAnulacio = NULL" + dataCreacio;
        if(dataModificacio == null) txt = txt + "\n dataModificacio = NULL" + dataCreacio;
return txt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
}
