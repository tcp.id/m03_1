package UF1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class IKS {
    public static void main(String[] args) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        Oficial capita = new Oficial("001", "a", false, 1, 1, true, "capitan",LocalDateTime.parse("15-08-1954 00:01", formatter));
        Oficial mariner_2_3 = new Oficial("001-A", "Martok", true,3, 1, true, "Mariner",LocalDateTime.parse("26-12-1981 13:42", formatter));

        System.out.println(capita.departament);
        System.out.println(capita.getDescripFeina());
        System.out.println("Si = protected == PACKAGE // NO = private --> GETTER --CLASSE");
        capita.imprimirDadesTripulant();
        capita.departament = 10;

        Oficial ofiOfi = new Oficial("001", "a", false, 1, 1, true, "capitan",LocalDateTime.parse("15-08-1954 00:01", formatter));
        Tripulant ofiTrip = new Oficial("001", "a", false, 1, 1, true, "capitan",LocalDateTime.parse("15-08-1954 00:01", formatter));
        ofiOfi.saludar();
        ofiTrip.saludar();








    }
}
