package UF1;

 class LlocServei {
    private final int ID;
    private String nom;
    private String descrip;

    public LlocServei(int ID, String nom, String descrip) {
        this.ID = ID;
        this.nom = nom;
        this.descrip = descrip;
    }

     public int getID() {
         return ID;
     }

     public String getNom() {
         return nom;
     }

     public void setNom(String nom) {
         this.nom = nom;
     }

     public String getDescrip() {
         return descrip;
     }

     public void setDescrip(String descrip) {
         this.descrip = descrip;
     }
 }
