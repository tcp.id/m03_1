package UF1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.concurrent.TimeUnit;

public class Waypoint {
    public static CompRendiment inicialitzarComprovacioRendiment() {
        CompRendiment comprovacioRendimentTmp = new CompRendiment();
        return comprovacioRendimentTmp;
    }

    public static CompRendiment comprovarRendimentInicialitzacio(int nObjCrear, CompRendiment compRendTmp) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        long timeI = System.nanoTime();
        long timeF = System.nanoTime();
        long time = timeF - timeI;
        long timeSeconds = TimeUnit.MILLISECONDS.convert(time, TimeUnit.NANOSECONDS);

        int[] coordTmp = new int[]{0, 0, 0};

        for (int i = 0; i < nObjCrear; i++) {
            compRendTmp.llistaArrayList.add(new Waypoint_dades(0, "Òrbita de la Terra", coordTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        }
        System.out.println("Temps per a insertar " + nObjCrear + " waypoints en l'ArrayList: " + timeSeconds);

        for (int j = 0; j < nObjCrear; j++) {
            compRendTmp.llistaLinkedList.add(new Waypoint_dades(0, "Òrbita de la Terra", coordTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        }
        System.out.println("Temps per a insertar " + nObjCrear + " waypoints en el LinkedList: " + timeSeconds);

        return compRendTmp;
    }

    public static CompRendiment comprovarRendimentInsercio(CompRendiment compRendimentT) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

        long timeI = System.nanoTime();
        long timeF = System.nanoTime();
        long time = timeF - timeI;
        long timeSeconds = TimeUnit.MILLISECONDS.convert(time, TimeUnit.NANOSECONDS);
        int prinLlista = 0;
        int mitadLlista = compRendimentT.llistaArrayList.size() / 2;
        int finLlista = compRendimentT.llistaArrayList.size();

        int[] coordenadesTmp = new int[]{0, 0, 0};

        System.out.println("mitadLista" + mitadLlista);

        System.out.println(" ARRAYLIST: \n add 1 posicion (0)\n");
        compRendimentT.llistaArrayList.add(prinLlista, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);
        System.out.println("add 1/2 pos\n");
        compRendimentT.llistaArrayList.add(mitadLlista, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);
        System.out.println("add fin(size()) pos\n");
        compRendimentT.llistaArrayList.add(finLlista, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);

        int mitadLlistaLin = compRendimentT.llistaLinkedList.size() / 2;
        int finLlistaLin = compRendimentT.llistaLinkedList.size();

        System.out.println(" LINKEDLIST: \n add 1 posicion (0)\n");
        compRendimentT.llistaLinkedList.add(prinLlista, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);
        System.out.println("add 1/2 pos\n");
        compRendimentT.llistaLinkedList.add(mitadLlistaLin, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);
        System.out.println("add fin(size()) pos\n");
        compRendimentT.llistaLinkedList.add(finLlistaLin, new Waypoint_dades(0, "Òrbita de la Terra", coordenadesTmp, true, LocalDateTime.parse("15-08-1954 00:01", formatter), null, null));
        System.out.println(time);

        return compRendimentT;
    }


    public static CompRendiment modificarWaypoints(CompRendiment compRendimentT) {
        List<Integer> idsPerArrayList = new ArrayList<>();

        for (int i = 0; i < compRendimentT.llistaArrayList.size(); i++) {
            idsPerArrayList.add(i);
        }

        int ult = idsPerArrayList.size() - 1;
        int prim = idsPerArrayList.get(0);

        System.out.println("primer element: \n" + prim + "ultim element: \n" + ult + "\n APARTAT 2\n");

        for (int i : idsPerArrayList) {
            System.out.println("\nABANS:" + i + compRendimentT.llistaArrayList.get(i).getId());
            compRendimentT.llistaArrayList.get(i).setId(i);
            System.out.println("\nDESPRES:" + i + compRendimentT.llistaArrayList.get(i).getId());
        }

        System.out.println("\n________MODO 1________");
        for (Waypoint_dades a : compRendimentT.llistaArrayList) {
            System.out.println("\nID = " + a.getId() + " nom = " + a.getNom());
        }

        System.out.println("\n________MODO 2________");

        Iterator<Waypoint_dades> itera = compRendimentT.llistaArrayList.iterator();
        Waypoint_dades aux;
        while (itera.hasNext()) {
            aux = itera.next();
            System.out.println("\nID = " + aux.getId() + " nom = " + aux.getNom());
        }
        //cambio id
        compRendimentT.llistaLinkedList.clear();
        compRendimentT.llistaLinkedList.addAll(compRendimentT.llistaArrayList);

        //cambio nombre > 5
        for (Waypoint_dades w : compRendimentT.llistaArrayList) {
            if (w.getId() > 5) {
                w.setNom("Orbita");
            }
        }

        //recorro e imprimo
        for (int i = 0; i > compRendimentT.llistaArrayList.size(); i++) {
            System.out.println("cambio en el punto con id =" + compRendimentT.llistaArrayList.get(i).getId() + " nom = " + compRendimentT.llistaArrayList.get(i).getNom());
        }

        //cambio nombre < 5 "Lagrange" + iterator
        itera = compRendimentT.llistaArrayList.iterator();
        while (itera.hasNext()) {
            aux = itera.next();
            if (aux.getId() < 5) {
                aux.setNom("Lagrange");
            }
            System.out.println("\nID = " + aux.getId() + " nom = " + aux.getNom());
        }
        return compRendimentT;
    }


    public static CompRendiment esborrarWaypoints(CompRendiment compRendimentT) {

      /*
        Waypoint_dades way = null;
        int z;

    for (Waypoint_dades zw : compRendimentT.llistaArrayList){
    z = way.getId();
    if(z < 6){
        compRendimentT.llistaArrayList.remove(z);
        }
    }
    si quito un elemento de array peta
*/
        Iterator<Waypoint_dades> it = compRendimentT.llistaArrayList.iterator();
        Waypoint_dades aux2;

        while (it.hasNext()) {
            aux2 = it.next();
            if (aux2.getId() > 4) {
                it.remove();
            }
        }
        ListIterator<Waypoint_dades> ut = compRendimentT.llistaLinkedList.listIterator();
        while (ut.hasNext()) {
            aux2 = ut.next();
            if (aux2.getId() == 2) {
                ut.remove();
            }

        }

        ut = compRendimentT.llistaLinkedList.listIterator(compRendimentT.llistaLinkedList.size() - 1);
        while (ut.hasPrevious()) {
            aux2 = ut.previous();
            System.out.println("ID = " + aux2.getId() + "nom=" + aux2.getNom());
        }
        return compRendimentT;
    }

    public static CompRendiment modificarCoordenadesINomDeWaypoints(CompRendiment compRendimentT) {
        return compRendimentT;

    }

}



