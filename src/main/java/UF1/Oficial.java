package UF1;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Oficial extends Tripulant implements IKSCONSTANT {
    private boolean serveiEnElPont;
    private String descripFeina;

    public Oficial(String id, String nom, boolean actiu, int departament, int llocServei, boolean serveiEnElPont, String descripFeina, LocalDateTime dataAlta) {
        super(id, nom, actiu, departament, llocServei, dataAlta);
        this.serveiEnElPont = serveiEnElPont;
        this.descripFeina = descripFeina;
    }



    public void setServeiEnElPont(boolean serveiEnElPont) {
        this.serveiEnElPont = serveiEnElPont;
    }

    public void setDescripFeina(String descripFeina) {
        this.descripFeina = descripFeina;
    }

    public boolean isServeiEnElPont() {
        return serveiEnElPont;
    }

    public String getDescripFeina() {
        return descripFeina;
    }

    @Override
    protected void imprimirDadesTripulant() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
        System.out.println( "DADES TRIPULANT: " +
                        "\nBandol: " + bandol +
                        "\nID: "  + super.id +
                        "\nNom: " + nom +
                        "\nActiu: " + actiu +
                        "\nDepartament: " + departament +
                        "\nLLoc de servei: " + super.getLlocServei() +
                        "\nDescripció de la feina que fa: " + descripFeina +
                        "\nServeix en el pont?: " + serveixEnElPont() +
                        "\nData d'alta: " + dataAlta.format(formatter)
        );
    }
    private String serveixEnElPont() {
        return serveiEnElPont ? "SI" : "NO";
    }

    protected void saludar() {
        System.out.println("Hola super");
    }

    @Override
    public String toString() {
        return "Oficial{" +
                "serveiEnElPont=" + serveiEnElPont +
                ", descripFeina='" + descripFeina + '\'' +
                ", id='" + id + '\'' +
                ", nom='" + nom + '\'' +
                ", actiu=" + actiu +
                ", departament=" + departament +
                ", llocServei=" + llocServei +
                ", dataAlta=" + dataAlta +
                '}';
    }
}
